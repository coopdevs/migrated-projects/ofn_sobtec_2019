# OFN Sobtec 2019

Diapositives de la presentació sobre Open Food Network al Sobtec 2019

* [Diapositives en PDF](https://speakerdeck.com/coopdevs/ofn-sobtec-2019?slide=25)

Fet amb [slides_template](https://github.com/coopdevs/slides_template) i [Backslide](https://github.com/sinedied/backslide).

### Exportar a HTML

```sh
$ npx bs export presentation.md
```

Això us generarà l'arxiu `dist/presentation.html`.

### Exportar a PDF

```sh
npx bs pdf
```

Això us generarà un pdf a la carpeta `pdf/`. Comprimiu-lo (pot arribar a pesar molt) i penjeu-lo al [compte de Speakerdeck de Coopdevs](speakerdeck.com/coopdevs).

Més detalls a: https://github.com/coopdevs/slides_template#usage
